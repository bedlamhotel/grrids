#!/usr/bin/env bash

# Define colour vars.
warn="\e[38;5;220m"
clear="\e[0m"

# Compare yarn versions--but only major and minor version, not patch.
yarn_version_dev="1.22"
yarn_version_ci=$(yarn --version | cut -d '.' -f 1-2)

if [ "$yarn_version_dev" != "$yarn_version_ci" ]; then
  echo -e "${warn}WARNING: the CI yarn version is \"${yarn_version_ci}\" but the dev yarn version is \"${yarn_version_dev}\"${clear}"
  exit 1
else:
  exit 0
fi
