# Grrids


[![pipeline status](https://gitlab.com/bedlamhotel/grrids/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/bedlamhotel/grrids/-/commits/main)

"Grrids" is a simple, flexible, and extensible grid system based on CSS grids.
It's under 3k minified--which might be small enough to use inline.

CSS' native grid capabilities do all the heavy lifting. This project only offers
a set of useful classes and css presets for common situations. But since it's
based on CSS' own amazing grid tools, it can be simply extended to create more
complex grid-based layouts.

See the demo at https://bedlamhotel.gitlab.io/grrids.

## Features

The basic library styles create a 48 column grid for maximum divisibility into
twelfths, eights, sixths, quarters, thirds and halves. A few utility classes
on top of the grid prove the following capabilities:

### 1. Minimal markup one-row grids of _n_ equal parts

![Four equal cell grid](screenshots/regular-grid.png)

Creating a simple one-row grid divided into equal parts can be done like this:

```html
<!-- Grid in four equal parts -->
<div class="grct">
  <div class="grr--quarters">
    <div>1/4</div>
    <div>1/4</div>
    <div>1/4</div>
    <div>1/4</div>
  </div>
</div>
```

The same grid could also be achieved this way:

```html
<!-- Grid in four equal parts -->
<div class="grct">
  <div class="grr">
    <div class="gcc--1-4">1/4</div>
    <div class="gcc--1-4">1/4</div>
    <div class="gcc--1-4">1/4</div>
    <div class="gcc--1-4">1/4</div>
  </div>
</div>
```

### 2. Minimal markup one-row grids of two unequal parts

![Two unequal cell grid](screenshots/fractional-grid.png)

```html
<!-- Grid divided into two parts, 2/3 and 1/3 -->
<div class="grct">
  <div class="grr">
    <div class="gcc--2-3">2/3</div>
    <div class="gcc--1-3">1/3</div>
  </div>
</div>
```

### 3. Minimal markup one-row grids with one centered cell

![One cell centered grid](screenshots/centered-grid.png)

```html
<!-- Grid with one 7/12 width cell centered -->
<div class="grct">
  <div class="grr--centered">
    <div class="gcc--7-12">1/3</div>
  </div>
</div>
```

### 4. Easy custom grids

![Fully customized grid](screenshots/custom-grid.png)

The utility classes work well for setting the widths of cells in complex grids
as well--note that this is the first example requiring any custom CSS to build:

```css
.grct { --gr-gap: 5px; }

.grr { grid-auto-rows: 4vh; }

.grc--1-3 { grid-row-start: 1; grid-row-end: 2; }

.grc--full { grid-row-start: 2; grid-row-end: 6; }

.grc--1-2:nth-child(5) { grid-row-start: 6; grid-row-end: 9; }

.grc--1-2:nth-child(6) { grid-row-start: 6; grid-row-end: 8; }
```

```html
<!-- A complex grid with 9 cells spanning various columns and rows -->
<div class="grct grg__debug--twelfths">
  <div class="grr grr__debug grc__debug">
    <div class="grc--1-3"><sup>1</sup>⁄<sub>3</sub></div>
    <div class="grc--1-3"><sup>1</sup>⁄<sub>3</sub></div>
    <div class="grc--1-3"><sup>1</sup>⁄<sub>3</sub></div>
    <div class="grc--full"><sup>12</sup>⁄<sub>12</sub> (Full)</div>
    <div class="grc--1-2"><sup>1</sup>⁄<sub>2</sub></div>
    <div class="grc--1-2"><sup>1</sup>⁄<sub>2</sub></div>
    <div class="grc--1-8"><sup>1</sup>⁄<sub>8</sub></div>
    <div class="grc--1-4"><sup>1</sup>⁄<sub>4</sub></div>
    <div class="grc--1-8"><sup>1</sup>⁄<sub>8</sub></div>
  </div>
</div>
```

### Classes

As mentioned above, the set of classes for setting grid-cell sizes is based on
a 48 column grid which allows the use of twelfths, eighths, sixths, quarters,
thirds, and halves (it could also be extended to offer sixteenths or twenty-
fourths).

But to provide classes for every possible fraction would require thirty-five
classes (12 + 8 + 6 + 4 + 3 + 2). Even if we used a single class to cover all
cases like <sup>12</sup>&frasl;<sub>12</sub>, <sup>8</sup>&frasl;<sub>8</sub>,
we'd still need thirty.

So to keep the library small, class names rely on the [lowest common denominator](https://en.wikipedia.org/wiki/Lowest_common_denominator#Role_in_arithmetic_and_algebra). This allows us to
base all the classes on just fifteen fractions plus one for full-width:

1. `1/12`
2. `1/8`
3. `1/6` (`2/12`)
4. `1/4` (`3/12`, `2/8`)
5. `1/3` (`4/12`)
6. `3/8`
7. `5/12`
8. `1/2` (`6/12`, `4/8`, `2/4`)
9. `7/12`
10. `5/8`
11. `2/3` (`8/12`, `4/6`)
12. `3/4` (`9/12`, `6/8`)
13. `5/6` (`10/12`)
14. `7/8`
15. `11/12`
16. `12/12`

In practice, we have more than sixteen classes because we provide classes for
centering, and space-saving classes that can be applied at the row-level instead
of on individual grid cells. The declarations can be seen in context [in the
`grid.css` file](css/grid.css), but the set of class names is:

| Class name       | Used on   | Function |
|------------------|-----------|----------|
| `.grct`          | Container | Container class, used to define vars for the number of grid columns, and the grid gap. |
| `.grr`           | Row       | Used to define basic grid properties for all default one-row grids. |
| `.grr--full`     | Row       | Used to define the span of grid columns for a full-width cell. |
| `.grr--centered` | Row       | Used to center a single cell in a grid row. |
| `.grr--twelfths` | Row       | Used to create a row with twelve equally-sized cells. |
| `.grc--1-12`     | Cell      | Used to give one cell a width 1/12 of the row's width. |
| `.grc--5-12`     | Cell      | Used to give one cell a width 5/12 of the row's width. |
| `.grc--7-12`     | Cell      | Used to give one cell a width 7/12 of the row's width. |
| `.grc--11-12`    | Cell      | Used to give one cell a width 11/12 of the row's width. |
| `.grr--eighths`  | Row       | Used to create a row with eight equally-sized cells. |
| `.grc--1-8`      | Cell      | Used to give one cell a width 1/8 of the row's width. |
| `.grc--3-8`      | Cell      | Used to give one cell a width 3/8 of the row's width. |
| `.grc--5-8`      | Cell      | Used to give one cell a width 5/8 of the row's width. |
| `.grc--7-8`      | Cell      | Used to give one cell a width 7/8 of the row's width. |
| `.grr--sixths`   | Row       | Used to create a row with six equally-sized cells. |
| `.grc--1-6`      | Cell      | Used to give one cell a width 1/6 of the row's width. |
| `.grc--5-6`      | Cell      | Used to give one cell a width 5/6 of the row's width. |
| `.grr--quarters` | Row       | Used to create a row with four equally-sized cells. |
| `.grr--fourths`  | Row       | Used to create a row with four equally-sized cells. |
| `.grc--1-4`      | Cell      | Used to give one cell a width 1/4 of the row's width. |
| `.grc--3-4`      | Cell      | Used to give one cell a width 3/4 of the row's width. |
| `.grr--thirds`   | Row       | Used to create a row with three equally-sized cells. |
| `.grc--1-3`      | Cell      | Used to give one cell a width 1/3 of the row's width. |
| `.grc--2-3`      | Cell      | Used to give one cell a width 2/3 of the row's width. |
| `.grr--halves`   | Row       | Used to create a row with two equally-sized cells. |
| `.grc--1-2`      | Cell      | Used to give one cell a width 1/2 of the row's width. |

## Why "Grrids"?

Because naming things is hard. And because the row class name is `grr` (`gr` for
'grid', and `r` for 'row').
